from django.urls import path
from receipts.views import (
    show_receipts,
    create_receipts,
    expense_category,
    account_list,
)

urlpatterns = [
    path("", show_receipts, name="home"),
    path("create/", create_receipts, name="create_receipt"),
    path("categories/", expense_category, name="category_list"),
    path("accounts/", account_list, name="account_list"),
]
