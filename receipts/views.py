from django.shortcuts import render, redirect
from receipts.models import Receipt, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm

# Create your views here.


@login_required
def show_receipts(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {"receipts": receipts}

    return render(request, "receipts/home.html", context)


@login_required
def create_receipts(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)

        if form.is_valid():
            receipt = form.save(False)
            request.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()

    context = {"form": form}
    return render(request, "receipts/create.html", context)


@login_required
def expense_category(request):

    context = {
        "expense": request.user.categories.all(),
    }
    return render(request, "receipts/expense.html", context)


def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "accounts": accounts,
    }
    return render(request, "receipts/account.html", context)
